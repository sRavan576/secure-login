package com.example.securelogin.Activities;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.Models.UserItem;
import com.example.securelogin.R;
import com.example.securelogin.SQLite.AttemptsDatabaseHelper;
import com.example.securelogin.SQLite.DatabaseHelper;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    // Delcaring all the required elements globally
    SQLiteOpenHelper openHelper;
    SQLiteOpenHelper attemptsHelper;
    SQLiteDatabase db, attemptsDb;
    EditText etEmail,etPassword;
    Button btLogin;
    TextView clickRegister;
    String stEmail, stPassword;
    int failedLoginAttemtes = 0;

//    private static final int ADMIN_++++INTENT = 15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//    we are checking the sharedpreferences to find out wheather the credentials are available..
        SharedPreferences sp= getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
        if((sp.getString("EMAIL","")) != null && !(sp.getString("EMAIL","").isEmpty())) {
            Intent i = new Intent(LoginActivity.this, NotesActivity.class);
            startActivity(i);
        }

        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btLogin = (Button)findViewById(R.id.btLogin);
        clickRegister = (TextView)findViewById(R.id.clickRegister);


        openHelper = new DatabaseHelper(this,"users",null,1);
        attemptsHelper = new AttemptsDatabaseHelper(this,"attempts",null,1);
        db=openHelper.getWritableDatabase();
        db=openHelper.getReadableDatabase();

        attemptsDb=attemptsHelper.getWritableDatabase();
        attemptsDb=attemptsHelper.getReadableDatabase();

//        Click event for Login
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db=openHelper.getReadableDatabase();
                stEmail = etEmail.getText().toString();
                stPassword = etPassword.getText().toString();

//      validations
                if (stEmail.isEmpty()){
                    etEmail.setError("Name cannot be empty");
                }
                if (stPassword.isEmpty()){
                    etPassword.setError("Mail cannot be empty");
                }
// calling method for login
                checkLogin(stEmail, stPassword);

//                Toast.makeText(getApplicationContext(), "register successfully",Toast.LENGTH_LONG).show();
            }
        });

//        click event for redirecting to Registration Page
        clickRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        }


// method to check user credentials and redirect to home
    public void checkLogin(String email,String password){

        String sqlCommand ="select id,name,email,password from users where email='"+email+"' and password ='"+password+"' order by id desc limit 1";
        Cursor cursor = db.rawQuery(sqlCommand,null);
        UserItem userData = getUserDetails(cursor);

        if(stEmail.equals(userData.Email)&& stPassword.equals(userData.Password)){

            failedLoginAttemtes = 0;


            // storing using credentials in shared preferences
            SharedPreferences sp=getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
            SharedPreferences.Editor editor=sp.edit();
            editor.putString("EMAIL",stEmail);
            editor.putInt("USERID",userData.getId());
            editor.commit();

            Intent intent = new Intent(LoginActivity.this,NotesActivity.class);
            startActivity(intent);
        }
        else {

            // to store exceeded login attempts in log
            if(failedLoginAttemtes >= 3){
                Snackbar.make(findViewById(R.id.login_layout),"Exceeded Maximum Login Attempts",Snackbar.LENGTH_SHORT).setAction("Action",null).show();

                String sql = "insert into loginattempts (userid) values(0)";
                attemptsDb.execSQL(sql);

                return;
            }
            failedLoginAttemtes = failedLoginAttemtes + 1;
            Snackbar.make(findViewById(R.id.login_layout),"Email or Password is Incorrect",Snackbar.LENGTH_SHORT).setAction("Action",null).show();

        }
    }


    public UserItem getUserDetails(Cursor c){

        UserItem user = new UserItem();

        while (c.moveToNext())
        {
            Integer id = c.getInt(c.getColumnIndex("id"));
            String name = c.getString(c.getColumnIndex("name"));
            String email = c.getString(c.getColumnIndex("email"));
            String password = c.getString(c.getColumnIndex("password"));

            try
            {
                user.setEmail(email);
                user.setId(id);
                user.setName(name);
                user.setPassword(password);
            }
            catch (Exception e) {
                Log.e("Cursor", "Error " + e.toString());
            }

        }
        return user;
    }
}
