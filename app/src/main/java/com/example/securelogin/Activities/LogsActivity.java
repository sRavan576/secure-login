package com.example.securelogin.Activities;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.securelogin.Adapters.AttemptsAdapter;
import com.example.securelogin.Models.AttemptsItem;
import com.example.securelogin.R;
import com.example.securelogin.SQLite.AttemptsDatabaseHelper;

import java.util.ArrayList;

public class LogsActivity extends AppCompatActivity {

    private RecyclerView  logsRecyclerView;
    private RecyclerView.Adapter  logsAdapter;
    LinearLayoutManager logsManager;
    Integer userId;
    SQLiteDatabase userDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);

        SharedPreferences sp= getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
        userId = sp.getInt("USERID",0);

        AttemptsDatabaseHelper adHelper = new AttemptsDatabaseHelper(this,"attempts",null,1);
        userDb = adHelper.getReadableDatabase();
        userDb = adHelper.getWritableDatabase();

        logsRecyclerView = (RecyclerView)findViewById(R.id.logsRecyclerview);
        logsManager = new LinearLayoutManager(this);
        logsManager.setOrientation(LinearLayoutManager.VERTICAL);
        logsRecyclerView.setLayoutManager(logsManager);

        String sqlCommand ="select id,attempttime from loginattempts";
        Cursor attemptsCursor = userDb.rawQuery(sqlCommand,null);
        ArrayList<AttemptsItem> attempts = getAttemptsValues(attemptsCursor);
        logsAdapter = new AttemptsAdapter(attempts,this);
        logsRecyclerView.setAdapter(logsAdapter);
        attemptsCursor.close();

    }

    public ArrayList<AttemptsItem> getAttemptsValues(Cursor c){

        ArrayList<AttemptsItem> list = new ArrayList<AttemptsItem>();

        while (c.moveToNext())
        {
            Integer id = c.getInt(c.getColumnIndex("id"));
            String attemptTime = c.getString(c.getColumnIndex("attempttime"));

            try
            {
                AttemptsItem obj = new AttemptsItem();
                obj.setAttempsId(id);
                obj.setAttemptTime(attemptTime);
                list.add(obj);
            }
            catch (Exception e) {
                Log.e("Cursor", "Error " + e.toString());
            }

        }
        return list;
    }
}
