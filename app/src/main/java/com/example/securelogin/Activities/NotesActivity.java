package com.example.securelogin.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.securelogin.Adapters.NotesAdapter;
import com.example.securelogin.Dialogs.NotesDialog;
import com.example.securelogin.Dialogs.OptionsDialog;
import com.example.securelogin.Interfaces.INotesAdapter;
import com.example.securelogin.Interfaces.INotesDialog;
import com.example.securelogin.Interfaces.IOptionsDialog;
import com.example.securelogin.Models.AttemptsItem;
import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.R;
import com.example.securelogin.SQLite.AttemptsDatabaseHelper;
import com.example.securelogin.SQLite.MyHelper;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

public class NotesActivity extends AppCompatActivity implements INotesAdapter,INotesDialog,IOptionsDialog {


    public ArrayList<NotesItem> notesList = new ArrayList<NotesItem>();
    NotesItem notesItem = new NotesItem();
    SQLiteDatabase db;
    INotesDialog iNotesDialog;
    IOptionsDialog iOptionsDialog;
    INotesAdapter iNotesAdapter;
    NotesDialog dialog;
    OptionsDialog optionsDialog;
    FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView ;
    private RecyclerView.Adapter adapter ;
    LinearLayoutManager manager;
    Integer userId;

    AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

//     getting userId from sharedpreferences to store data based on userId

        SharedPreferences sp= getSharedPreferences("MYUSERDATA",MODE_PRIVATE);
        userId = sp.getInt("USERID",0);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);

        // declaring that interfaces belongs to this activity
        iNotesDialog = this;
        iNotesAdapter = this;
        iOptionsDialog = this;

        // Initializing floating action button and click event for it
        floatingActionButton=(FloatingActionButton)findViewById(R.id.floatingactionbutton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // calling method for adding new Document
                newDocument();

            }

        });


        // Database
        MyHelper object = new MyHelper(this,"Notes",null,1);
        db=object.getWritableDatabase();

        String sqlCommand ="select id,title,notes from notes where userid="+userId;
        Cursor cursor = db.rawQuery(sqlCommand,null);

        // calling method for adding each notes to list and sending into list(allNotes)
        ArrayList<NotesItem> allNotes = getAllValues(cursor);
        adapter = new NotesAdapter(allNotes,this,iNotesAdapter);
        recyclerView.setAdapter(adapter);
        cursor.close();

    }


    public ArrayList<NotesItem> getNotesList(){
        return  null;
    }


    // creation of icons on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // click events for icons on action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logout:
                alertDialog = new AlertDialog.Builder(NotesActivity.this);
                alertDialog.setTitle("EXIT");
                alertDialog.setMessage("Do you want to Logout ?");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();

                        // clears user credentials in shared preferences
                        SharedPreferences preferences =getSharedPreferences("MYUSERDATA", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        finish();

                        Intent intent=new Intent(NotesActivity.this,LoginActivity.class);
                        startActivity(intent);

                    }
                });

                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

                alertDialog.show();
                return super.onOptionsItemSelected(item);
            case R.id.log:
                Intent intent=new Intent(NotesActivity.this,LogsActivity.class);
                startActivity(intent);
                return super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

// method for adding notes
    @Override
    public void addNote(NotesItem notesItem) {

        String sql = "insert into notes (title, userid, notes) values('"+notesItem.NoteTitle+"',"+userId+",'"+notesItem.Notes+"')";
        db.execSQL(sql);
        dialog.dismiss();
        String sqlCommand ="select id,title,notes from notes where userid="+userId;
        Cursor  cursor = db.rawQuery(sqlCommand,null);
        ArrayList<NotesItem> allNotes = getAllValues(cursor);
        adapter = new NotesAdapter(allNotes,this,iNotesAdapter);
        recyclerView.setAdapter(adapter);
        cursor.close();

//        Snackbar.make(getActivity().findViewById(R.id.frameid), "Notes Added Succesfully!",Snackbar.LENGTH_SHORT).show();
    }

    // method for updating notes
    @Override
    public void updateNote(NotesItem notesItem) {
        String sql = "update notes set title='"+notesItem.NoteTitle+"',notes='"+notesItem.Notes+"' where id = "+notesItem.NoteId;
        db.execSQL(sql);
        dialog.dismiss();
        String sqlCommand ="select id,title,notes from notes where userid="+userId;
        Cursor  cursor = db.rawQuery(sqlCommand,null);
        ArrayList<NotesItem> allNotes = getAllValues(cursor);
        adapter = new NotesAdapter(allNotes,this,iNotesAdapter);
        recyclerView.setAdapter(adapter);
        cursor.close();

//        Snackbar.make(getActivity().findViewById(R.id.frameid), "Notes Updated Succesfully..!",Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void cancelNote() {
        dialog.dismiss();
    }


    // method for onclick of an item in list
    @Override
    public void itemClick(NotesItem item, int position) {


        optionsDialog = new OptionsDialog(this,item,iOptionsDialog);
        optionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionsDialog.show();
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int width = (int) (metrics.widthPixels * 1);
        optionsDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);


    }

    // method for editing notes
    @Override
    public void editNote(NotesItem notesItem) {
        optionsDialog.dismiss();
        dialog = new NotesDialog(this,notesItem,iNotesDialog,"update");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int width = (int) (metrics.widthPixels * 1);
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    // method for deleting notes
    @Override
    public void deleteNote(NotesItem notesItem) {
        String sql = " delete from notes where id = "+notesItem.NoteId;
        db.execSQL(sql);
        String sqlCommand ="select id,title,notes from notes where userid="+userId;
        Cursor  cursor = db.rawQuery(sqlCommand,null);
        ArrayList<NotesItem> allNotes = getAllValues(cursor);
        adapter = new NotesAdapter(allNotes,this,iNotesAdapter);
        recyclerView.setAdapter(adapter);
        cursor.close();
        optionsDialog.dismiss();

//        Snackbar.make(getActivity().findViewById(R.id.frameid), "Notes Deleted..!",Snackbar.LENGTH_SHORT).show();
    }


    // method for adding new document
    public void newDocument() {

        notesItem=new NotesItem();
        notesItem.NoteTitle="";
        notesItem.Notes="";
        dialog = new NotesDialog(this,notesItem,iNotesDialog,"insert");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int width = (int) (metrics.widthPixels * 1);
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);


    }


    public ArrayList<NotesItem> getAllValues(Cursor c){

        ArrayList<NotesItem> list = new ArrayList<NotesItem>();

        while (c.moveToNext())
        {
            Integer id = c.getInt(c.getColumnIndex("id"));
            String title = c.getString(c.getColumnIndex("title"));
            String notes = c.getString(c.getColumnIndex("notes"));

            try
            {
                NotesItem obj = new NotesItem();
                obj.setNoteId(id);
                obj.setNoteTitle(title);
                obj.setNotes(notes);
                list.add(obj);
            }
            catch (Exception e) {
                Log.e("Cursor", "Error " + e.toString());
            }

        }
        return list;
    }

}
