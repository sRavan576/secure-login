package com.example.securelogin.Activities;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.securelogin.Models.UserItem;
import com.example.securelogin.SQLite.DatabaseHelper;
import com.example.securelogin.R;
import com.example.securelogin.receivers.LoginAttemptsReceiver;

public class RegisterActivity extends AppCompatActivity {

// Declaring all the required elememts globally
    SQLiteOpenHelper openHelper;
    SQLiteDatabase db;
    EditText etEmail,etName,etPassword,etConformPassword;
    Button btRegister;
    Boolean checkUserData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


//      Asking Device Administration Permissions

//        ComponentName cn=new ComponentName(this, LoginAttemptsReceiver.class);
//        DevicePolicyManager mgr=
//                (DevicePolicyManager)getSystemService(DEVICE_POLICY_SERVICE);
//
//        if (mgr.isAdminActive(cn)) {
//        }
//        else {
//            Intent intent=
//                    new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, cn);
//            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                    getString(R.string.app_name));
//            startActivity(intent, ADMIN_INTENT);
//        }

//    setiing xml file to the Activity
        setContentView(R.layout.layout_register);

        etName = (EditText)findViewById(R.id.etName);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etConformPassword = (EditText)findViewById(R.id.etConformPassword);
        btRegister = (Button)findViewById(R.id.btRegister);

        // Database
        openHelper = new DatabaseHelper(this,"users",null,1);
        db=openHelper.getWritableDatabase();
        db=openHelper.getReadableDatabase();

//      Click event for Register

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUserData = true;
                db=openHelper.getWritableDatabase();
                String stName = etName.getText().toString();
                String stMailId = etEmail.getText().toString();
                String stPassword = etPassword.getText().toString();
                String stConformPassword = etConformPassword.getText().toString();

                String sqlCommand ="select id,name,email,password from users where email='"+stMailId+"' order by id desc limit 1";
                Cursor cursor = db.rawQuery(sqlCommand,null);
                UserItem userData = getUserDetails(cursor);

                // if user forgets to fill all the fields , it will display error messages as following
                if (stName.isEmpty()){
                    etName.setError("Name cannot be empty");
                    checkUserData = false;
                }
                if (stMailId.isEmpty()){
                    etEmail.setError("Mail cannot be empty");
                    checkUserData = false;
                }
                if (stPassword.isEmpty()){
                    etPassword.setError("Password cannot be empty");
                    checkUserData = false;
                }
                if (stConformPassword.isEmpty()){
                    etConformPassword.setError("field cannot be empty");
                    checkUserData = false;
                }
                if (!(stPassword.equals(stConformPassword)))
                {
                    etConformPassword.setError("Passwords Doesn't Match");
                    checkUserData = false;
                }

                // it checks the userdata for same credentials,if those credentials are already available...then it displays an error message.
                if(checkUserData) {
                    if(etEmail.equals(userData.Email)){
                        Snackbar.make(findViewById(R.id.login_layout),"User Alredy Exists",Snackbar.LENGTH_SHORT).setAction("Action",null).show();
                    }
                    else {
                        //calling method for insertion
                        insertData(stName, stMailId, stPassword);
                        Toast.makeText(getApplicationContext(), "register successfully", Toast.LENGTH_SHORT).show();

                        // this is one of the major pieces of code which redirects the user form one activity to another activity.
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
//  this method is used to insert user credentials into the database
    public void insertData(String stName,String stMailId,String stPassword) {
        String sql = "insert into users (name ,email,password) values('"+stName+"','"+stMailId+"','"+stPassword+"')";
        db.execSQL(sql);
    }

    // this method is used to get the user details from the database to check wheather the similar details exist or not.
    public UserItem getUserDetails(Cursor c){

        UserItem user = new UserItem();

        while (c.moveToNext())
        {
            Integer id = c.getInt(c.getColumnIndex("id"));
            String name = c.getString(c.getColumnIndex("name"));
            String email = c.getString(c.getColumnIndex("email"));
            String password = c.getString(c.getColumnIndex("password"));

            try
            {
                user.setEmail(email);
                user.setId(id);
                user.setName(name);
                user.setPassword(password);
            }
            catch (Exception e) {
                Log.e("Cursor", "Error " + e.toString());
            }

        }
        return user;
    }

}
