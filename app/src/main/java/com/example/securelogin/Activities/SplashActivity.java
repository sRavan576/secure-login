package com.example.securelogin.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.securelogin.R;

public class SplashActivity extends AppCompatActivity {

    ImageView ivSplash;
    Animation animation;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ivSplash = (ImageView)findViewById(R.id.ivSplash);
        ivSplash.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(this,R.anim.animation);
        ivSplash.startAnimation(animation);

        intent = new Intent(this,LoginActivity.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        },3000);
    }
}
