package com.example.securelogin.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.securelogin.Interfaces.INotesAdapter;
import com.example.securelogin.Models.AttemptsItem;
import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.R;

import java.util.ArrayList;

public class AttemptsAdapter extends RecyclerView.Adapter<AttemptsAdapter.ViewHolder> {

    public ArrayList<AttemptsItem> list;
    private Context context;

    public AttemptsAdapter(ArrayList<AttemptsItem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public AttemptsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.logslist,parent,false);
        return new AttemptsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AttemptsAdapter.ViewHolder holder, final int position) {

        final AttemptsItem item = list.get(position);

        holder.tvAttemptNote.setText("Last wrong attempt  at  "+item.getAttemptTime());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvAttemptNote ;


        public ViewHolder(View view){
            super(view);

            tvAttemptNote = (TextView) view.findViewById(R.id.logStatus);
        }
    }
}


