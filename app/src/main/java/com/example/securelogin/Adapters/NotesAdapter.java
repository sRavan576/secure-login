package com.example.securelogin.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.securelogin.Interfaces.INotesAdapter;
import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.R;

import java.security.AccessControlContext;
import java.util.ArrayList;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    public INotesAdapter iNotesAdapter;
    public ArrayList<NotesItem> list;
    private Context context;

    public NotesAdapter(ArrayList<NotesItem> list, Context context, INotesAdapter iNotesAdapter) {
        this.list = list;
        this.context = context;
        this.iNotesAdapter = iNotesAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.noteslist,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final NotesItem item = list.get(position);

        holder.tvNoteTitle.setText(item.getNoteTitle());
        holder.tvNotes.setText(item.getNotes());

        holder.cvNotesCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iNotesAdapter.itemClick(item,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvNoteTitle ;
        public TextView tvNotes;
        public CardView cvNotesCard;



        public ViewHolder(View view){
            super(view);

            tvNoteTitle = (TextView) view.findViewById(R.id.noteTitle);
            tvNotes = (TextView) view.findViewById(R.id.noteDescription);
            cvNotesCard=(CardView) view.findViewById(R.id.notesCard);
        }
    }
}

