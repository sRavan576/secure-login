package com.example.securelogin.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.securelogin.Interfaces.INotesDialog;
import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.R;

public class NotesDialog extends Dialog
{
    Context context;
    NotesItem notesItem = new NotesItem();
    INotesDialog iNotesDialog;
    EditText et_title;
    EditText et_description;
    Button bt_savebutton;
    Button bt_cancelbutton;
    String type;

    public NotesDialog(Context context , NotesItem notesItem,INotesDialog iNotesDialog, String type) {
        super(context);
        this.notesItem=notesItem;
        this.context = context;
        this.iNotesDialog = iNotesDialog;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noteseditordialog);

        et_title=(EditText)findViewById(R.id.title);
        et_description=(EditText)findViewById(R.id.description);
        bt_savebutton=(Button)findViewById(R.id.savebutton);
        bt_cancelbutton=(Button)findViewById(R.id.cancelbutton);

        et_title.setText(notesItem.getNoteTitle());
        et_description.setText(notesItem.getNotes());

        bt_savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notesItem.NoteTitle = et_title.getText().toString();
                notesItem.Notes = et_description.getText().toString();
                int count=0;
                if (notesItem.NoteTitle.trim().length()==0)
                {
                    et_title.setError("Title cannot be Empty");
                    count=1;
                }

                if (notesItem.Notes.trim().length()==0)
                {
                    et_description.setError("Description cannot be Empty");
                    count=1;
                }

                if (count == 0) {
                    if (type == "insert") {
                        iNotesDialog.addNote(notesItem);
                    } else {
                        iNotesDialog.updateNote(notesItem);
                    }
                }
            }
        });

        bt_cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iNotesDialog.cancelNote();
            }
        });
    }

}

