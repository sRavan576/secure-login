package com.example.securelogin.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.securelogin.Interfaces.IOptionsDialog;
import com.example.securelogin.Models.NotesItem;
import com.example.securelogin.R;

public class OptionsDialog extends Dialog {


    Context context;
    NotesItem notesItem = new NotesItem();
    IOptionsDialog iOptionsDialog;
    ImageView editNotes;
    ImageView deleteNotes;



    public OptionsDialog(Context context , NotesItem notesItem, IOptionsDialog ioptionsDialog) {
        super(context);
        this.notesItem=notesItem;
        this.context = context;
        this.iOptionsDialog = ioptionsDialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.optionsdialog);

        editNotes=(ImageView) findViewById(R.id.editNotes);
        deleteNotes=(ImageView) findViewById(R.id.deleteNotes);

        editNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               iOptionsDialog.editNote(notesItem);
            }
        });

        deleteNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOptionsDialog.deleteNote(notesItem);
            }
        });

        return ;
    }

}
