package com.example.securelogin.Interfaces;

import com.example.securelogin.Models.NotesItem;

public interface INotesAdapter {
    public  void itemClick(NotesItem item, int position);
}
