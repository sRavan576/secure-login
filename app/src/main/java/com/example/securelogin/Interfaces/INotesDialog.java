package com.example.securelogin.Interfaces;

import com.example.securelogin.Models.NotesItem;

public interface INotesDialog {
    public void addNote(NotesItem notesItem);
    public void updateNote(NotesItem notesItem);
    public void cancelNote();
}
