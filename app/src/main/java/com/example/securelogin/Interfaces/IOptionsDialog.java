package com.example.securelogin.Interfaces;

import com.example.securelogin.Models.NotesItem;

public interface IOptionsDialog {
    public void editNote(NotesItem notesItem);
    public void deleteNote(NotesItem notesItem);
}
