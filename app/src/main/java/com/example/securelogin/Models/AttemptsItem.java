package com.example.securelogin.Models;

public class AttemptsItem {
    public int AttempsId;
    public int UserId;
    public String AttemptTime;

    public AttemptsItem() {

    }

    public AttemptsItem(int attempsId, int userId, String attemptTime) {
        AttempsId = attempsId;
        UserId = userId;
        AttemptTime = attemptTime;
    }

    public int getAttempsId() {
        return AttempsId;
    }

    public void setAttempsId(int attempsId) {
        AttempsId = attempsId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getAttemptTime() {
        return AttemptTime;
    }

    public void setAttemptTime(String attemptTime) {
        AttemptTime = attemptTime;
    }
}
