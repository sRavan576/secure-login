package com.example.securelogin.Models;

public class NotesItem {

    public int NoteId;
    public String NoteTitle;
    public String Notes;

    public int getNoteId() {
        return NoteId;
    }

    public void setNoteId(int noteId) {
        NoteId = noteId;
    }

    public String getNoteTitle() {
        return NoteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        NoteTitle = noteTitle;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public NotesItem() {

    }
    public NotesItem(int noteId, String noteTitle, String notes) {
        NoteId = noteId;
        NoteTitle = noteTitle;
        Notes = notes;
    }
}
