package com.example.securelogin.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AttemptsDatabaseHelper extends SQLiteOpenHelper {

    public AttemptsDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "create table loginattempts (id  Integer Primary key autoincrement, userid Integer, attempttime datetime default current_timestamp)";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP table if exists loginattempts";
        db.execSQL(sql);
        onCreate(db);
    }
}
