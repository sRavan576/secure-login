package com.example.securelogin.receivers;


import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.UserHandle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.securelogin.GMailSender;
import com.example.securelogin.SendMailTask;

public class LoginAttemptsReceiver extends DeviceAdminReceiver{

    private static final String KEY_ATTEMPTS_NO = "attempts_no";
    private static final int LIMIT = 3;

    public static final String ACTION_PASSWORD_FAILED = "android.app.action.ACTION_PASSWORD_FAILED";
    public static final String ACTION_PASSWORD_CHANGED = "android.app.action.ACTION_PASSWORD_CHANGED";
    public static final String ACTION_PASSWORD_SUCCEEDED = "android.app.action.ACTION_PASSWORD_SUCCEEDED";
    public static final String ACTION_DEVICE_ADMIN_ENABLED = "android.app.action.ACTION_DEVICE_ADMIN_ENABLED";

    @Override
    public void onEnabled(Context ctxt, Intent intent) {
        ComponentName cn=new ComponentName(ctxt, LoginAttemptsReceiver.class);
        DevicePolicyManager mgr=
                (DevicePolicyManager)ctxt.getSystemService(Context.DEVICE_POLICY_SERVICE);

        mgr.setPasswordQuality(cn,
                DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC);

        onPasswordChanged(ctxt, intent);
    }

    @Override
    public void onPasswordFailed(Context context, Intent intent) {

        DevicePolicyManager mgr = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        int no = mgr.getCurrentFailedPasswordAttempts();

        if (no >= 1) {
            try {
                openFrontFacingCamera();
                //new SendMailTask().execute();
            }catch (Exception e) {
                Log.e("SendMailTask", e.getMessage(), e);
            }
            return;
        }

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        int attempts = sharedPrefs.getInt(KEY_ATTEMPTS_NO, 0) + 1;

        if (attempts == LIMIT) {
            // Reset the counter
            sharedPrefs.edit().putInt(KEY_ATTEMPTS_NO, 0).apply();

            // Launch your activity
            // context.startActivity(new Intent(context, test.class));
            Toast.makeText(context, "start activity", Toast.LENGTH_LONG).show();
        } else {
            // Save the new attempts number
            sharedPrefs.edit().putInt(KEY_ATTEMPTS_NO, attempts).apply();
        }
    }

    @Override
    public void onPasswordSucceeded(Context context, Intent intent) {
        // Reset number of attempts
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPrefs.edit().putInt(KEY_ATTEMPTS_NO, 0).apply();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_PASSWORD_CHANGED.equals(action)) {
            onPasswordChanged(context, intent);
        } else if (ACTION_PASSWORD_FAILED.equals(action)) {
            onPasswordFailed(context, intent);
        } else if (ACTION_PASSWORD_SUCCEEDED.equals(action)) {
            onPasswordSucceeded(context, intent);
        } else if (ACTION_DEVICE_ADMIN_ENABLED.equals(action)) {
            onEnabled(context, intent);
        }
    }

    private Camera openFrontFacingCamera() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for ( int camIdx = 0; camIdx < cameraCount; camIdx++ ) {
            Camera.getCameraInfo( camIdx, cameraInfo );
            if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT  ) {
                try {
                    cam = Camera.open( camIdx );
                } catch (RuntimeException e) {
                    Log.i("Camera failed to open: ",e.getLocalizedMessage());
                }
            }
        }
        return cam;
    }
}
